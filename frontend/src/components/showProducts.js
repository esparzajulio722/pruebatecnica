import React,{ useEffect, useState} from "react";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

import { show_alerta } from "../functions";

const ShowProducts = () =>{
    const urlProductos = 'http://localhost/pruebatecnica/inventario/public/api/productos';
    const urlCategorias = 'http://localhost/pruebatecnica/inventario/public/api/categorias';
    const urlRegistrar = 'http://localhost/pruebatecnica/inventario/public/api/registro';
    const urlActualizar = 'http://localhost/pruebatecnica/inventario/public/api/actualizar';
    const urlEliminar = 'http://localhost/pruebatecnica/inventario/public/api/eliminar/';

    const [products,setProducts] = useState([]);
    const [categorias,setCategorias] = useState([]);
    const [id,setId] = useState('');
    const [sku,setSku] = useState('');
    const [name,setName] = useState('');
    const [descripcion,setDescripcion] = useState('');
    const [cantidad,setCantidad] = useState('');
    const [precio,setPrecio] = useState('');
    const [categoria,setCategoria] = useState('-1');
    const [estado,setEstado] = useState('C');
    const [operation,setOperation] = useState(1);
    const [title,setTitle] = useState('');

    useEffect( () =>{
        getCategorias();
        getProducts();
    },[]);

    const getProducts = async () => {
        const respuesta = await axios.get(urlProductos);
        setProducts(respuesta.data.data);
    }

    const getCategorias = async () => {
        const respuesta = await axios.get(urlCategorias);
        setCategorias(respuesta.data.data);
    }
    
    const openModal = (op,id,name) => {
        setId('');
        setName('');
        setOperation(op);
        if(op === 1){
            setTitle('Registrar Producto');
        }else{
            setTitle('Editar Producto');
            setId(id);
            setName(name);
        }
        window.setTimeout(function(){
            document.getElementById("nombre").focus();
        },500);
    }

    const validar = () =>{
        var parametros;
        var metodo;
        var url;
        if(sku.trim() === ''){
            show_alerta("Escribe el sku del producto",'warning');
        }else if(name.trim() === ''){
            show_alerta("Escribe el nombre del producto",'warning');
        }else if(categoria == '-1'){
            show_alerta("Selecciona una categoria",'warning');
        }else if(descripcion.trim() === ''){
            show_alerta("Escribe la descripción del producto",'warning');
        }else if(cantidad.trim() === ''){
            show_alerta("Ingresa la cantidad del producto",'warning');
        }else if(precio.trim() === ''){
            show_alerta("Ingresa el precio del producto",'warning');
        }else{
            if(operation === 1){
                parametros = {nombre: name.trim(),sku: sku.trim(),estado: estado,descripcion: descripcion.trim(),categoria_id: categoria.trim(),cantidad: cantidad,precio:precio};
                metodo = 'POST';
                url = urlRegistrar;
            }else{
                parametros = {id: id, nombre: name.trim(),sku: sku.trim(),estado: estado,descripcion: descripcion.trim(),categoria_id: categoria.trim(),cantidad: cantidad,precio:precio};
                metodo = 'POST';
                url = urlActualizar;
            }
            enviarSolicitud(metodo,parametros,url);
            setId('');
            setName('');
            setDescripcion('');
            setCantidad('');
            setPrecio('');
            setCategoria('-1');
            setSku('');
            setEstado('C')
        }
    }

    const updateCantidad = (id,cantidad,tipo) =>{
        if(tipo == '-'){
            cantidad --;   
        }else{
            cantidad ++;
        }
        document.getElementById(id).value = cantidad;
        var parametros = {id: id,cantidad: cantidad};
        enviarSolicitud('POST',parametros,urlActualizar);
    }

    const enviarSolicitud = async(metodo,parametros,url) =>{
        await axios({ method: metodo, url:url, data:parametros}).then(function(respuesta){
            var tipo = respuesta.status;
            var msj = respuesta.data.message;
            show_alerta(msj,'success');
            if(tipo === 200){
                document.getElementById('btnCerrar').click();
            }
            getProducts();
        }).catch(function(error){
            show_alerta('Error en solicitud.','error');
            console.log(error);
        });
    }
    
    const eliminarProducto = (id, name) =>{
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: '¿Seguro de eliminar?',
            icon: 'question',
            text:'No se podra dar marcha atras.',
            showCancelButton: true, confirmButtonText: 'Si, eliminar',cancelButtonText:'Cancelar' 
        }).then((result)=>{
            if(result.isConfirmed){
                setId(id);
                enviarSolicitud('DELETE',{id:id},urlEliminar+id);
            }else{
                show_alerta('El producto no fue eliminado.','info');
            }
        });
    }

    const openModalDetalle = (producto) => {
        setId(producto.id);
        setName(producto.nombre);
        setDescripcion(producto.descripcion);
        setCantidad(producto.cantidad);
        setCategoria(producto.categoria_id);
        setEstado(producto.estado);
        setPrecio(producto.precio);
        setSku(producto.sku);
    }

    return (
        <div className="App">
            <div className="container-fluid">
                <div className="row mt-3">
                    <div className="col-md-4 offset-4">
                        <div className="d-grid mx-auto">
                            <button onClick={()=>openModal(1)} className="btn btn-dark" data-bs-toggle='modal' data-bs-target='#modalProducts'>
                                <i className="fa-solid fa-circle-plus"></i>
                                Registrar Producto
                            </button>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 row">
                    {products.map((producto,i)=>
                        <div className="col-md-12 card mb-3 mt-5" style={{ maxWidth: '100%' }}>
                            <div className="row">
                                <div className="row no-gutters col-md-10">
                                    <div className="col-md-4">
                                        <img style={{ width: '50%' }} src="https://m.media-amazon.com/images/I/71iMKOqRF-L._AC_UF894,1000_QL80_.jpg" className="card-img" alt="..."/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="card-body">
                                            <h5 className="card-title">{producto.nombre}</h5>
                                            <p className="card-text">SKU: {producto.sku}</p>
                                            <p className="card-text text-warning">
                                                <i class="fa-solid fa-star"></i>
                                                <i class="fa-solid fa-star"></i>
                                                <i class="fa-solid fa-star"></i>
                                                <i class="fa-solid fa-star"></i>
                                                <i class="fa-solid fa-star"></i>
                                            </p>
                                            <p>
                                                {
                                                    producto.cantidad > 0 ? <p style={{ color: '#1FFF00' }}>In Stock</p> : <p style={{ color: 'red' }}>No Stock</p>
                                                }
                                            </p>
                                            <p>
                                                <div className="row">
                                                    <div className="col-md-1">
                                                        <button onClick={()=>updateCantidad(producto.id,producto.cantidad,'-')}  type="button" className="btn btn-secondary">-</button>
                                                    </div>
                                                    <div className="col-md-2">
                                                        <input disabled id={producto.id} onChange={(e) => setCantidad(e.target.value)} type="number" className="form-control" value={producto.cantidad}></input>
                                                    </div>
                                                    <div className="col-md-1">
                                                        <button onClick={()=>updateCantidad(producto.id,producto.cantidad,'+')}  type="button" className="btn btn-secondary">+</button>
                                                    </div>
                                                </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2 border">
                                    <div className="col-md-12 text-center">
                                        ${producto.precio}
                                    </div>
                                    <div className="col-md-12 text-center">
                                        
                                        {
                                            producto.estado === 'C' ? <p style={{ color: '#1FFF00' }}>Disponible</p> : <p style={{ color: 'red' }}>No Disponible</p>
                                        }
                                    </div>
                                    <div className="col-md-12 text-center">
                                        <button onClick={()=>eliminarProducto(producto.id,producto.nombre)}  type="button" className="btn btn-secondary" style={{ width: '100%' }}><i class="fa-solid fa-trash text-white"></i>Eliminar </button>
                                    </div>
                                    <div className="col-md-12 text-center mt-2">
                                        <button type="button" className="btn btn-primary" style={{ width: '100%' }}><i class="fa-solid fa-heart text-white"></i>Calificar </button>
                                    </div>
                                    <div className="col-md-12 text-center mt-2">
                                        <button onClick={()=>openModalDetalle(producto)} data-bs-toggle='modal' data-bs-target='#modalDetalles' type="button" className="btn btn-primary" style={{ width: '100%' }}><i class="fa-solid fa-eye text-white"></i>Detalles </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </div>
            <div id="modalProducts" className="modal fade" aria-hidden='true'>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <label className="h5">{title}</label>
                            <button className="btn-close" type='button' data-bs-dismiss='modal'></button>
                        </div>
                        <div className="modal-body">
                            <input type="hidden" id="id"></input>
                            <div className="input-group mb-3">
                                <input type="text" id="sku" className="form-control" placeholder="Sku" value={sku} onChange={(e) => setSku(e.target.value)}></input>
                            </div>
                            <div className="input-group mb-3">
                                <input type="text" id="nombre" className="form-control" placeholder="Nombre" value={name} onChange={(e) => setName(e.target.value)}></input>
                            </div>
                            <div className="input-group mb-3">
                                <select className="form-control" onChange={(e) => setCategoria(e.target.value)}>
                                    <option value={'-1'}>Selecciona una categoria</option>
                                    {categorias.map((categoria,i)=>
                                        <option value={categoria.id}>{categoria.nombre}</option>
                                    )}
                                </select>
                            </div>
                            <div className="input-group mb-3">
                                <input type="text" id="descripcion" className="form-control" placeholder="Descripcion" value={descripcion} onChange={(e) => setDescripcion(e.target.value)}></input>
                            </div>
                            <div className="input-group mb-3">
                                <input type="number" id="cantidad" className="form-control" placeholder="Cantidad" value={cantidad} onChange={(e) => setCantidad(e.target.value)}></input>
                            </div>
                            <div className="input-group mb-3">
                                <select className="form-control" onChange={(e) => setEstado(e.target.value)}>
                                    <option value={'C'}>Con inventario</option>
                                    <option value={'S'}>Sin inventario</option>
                                </select>
                            </div>
                            <div className="input-group mb-3">
                                <input type="number" id="precio" className="form-control" placeholder="$Precio" value={precio} onChange={(e) => setPrecio(e.target.value)}></input>
                            </div>
                            <div className="d-grid col-6 mx-auto">
                                <button onClick={() => validar()} className="btn btn-success">
                                    <i className="fa-solid fa-floppy-disk"></i>
                                    Guardar
                                </button>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button id="btnCerrar" type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="modalDetalles" className="modal fade" aria-hidden='true'>
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <label className="h5">{title}</label>
                            <button className="btn-close" type='button' data-bs-dismiss='modal'></button>
                        </div>
                        <div className="modal-body">
                        <div className="row">
                                <div className="row no-gutters col-md-12">
                                    <div className="col-md-4">
                                        <img style={{ width: '100%' }} src="https://m.media-amazon.com/images/I/71iMKOqRF-L._AC_UF894,1000_QL80_.jpg" className="card-img" alt="..."/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="card-body">
                                            <h5 className="card-title">{name}</h5>
                                            <p className="card-text">SKU: {sku}</p>
                                            <p className="card-text">SKU: {precio}</p>
                                            <p className="card-text text-warning">
                                                <i class="fa-solid fa-star"></i>
                                                <i class="fa-solid fa-star"></i>
                                                <i class="fa-solid fa-star"></i>
                                                <i class="fa-solid fa-star"></i>
                                                <i class="fa-solid fa-star"></i>
                                            </p>
                                            <p>
                                                {
                                                    estado === 'C' ? <p style={{ color: '#1FFF00' }}>Disponible</p> : <p style={{ color: 'red' }}>No Disponible</p>
                                                }
                                            </p>
                                            <p>{descripcion}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button id="btnCerrar" type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ShowProducts