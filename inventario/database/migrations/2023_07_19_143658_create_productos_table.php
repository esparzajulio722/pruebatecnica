<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku',250);
            $table->string('nombre',250);
            $table->integer('categoria_id')->unsigned();
            $table->text('descripcion');
            $table->float('precio')->defualt(0);
            $table->integer('cantidad')->default(0);
            $table->char('estado',5)->default('C')->comments('C con inventario, S sin inventario');
            $table->char('estatus',5)->default('A');
            $table->timestamps();

            $table->foreign('categoria_id')->references('id')->on('categorias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
