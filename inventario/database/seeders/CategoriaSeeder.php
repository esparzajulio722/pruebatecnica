<?php

namespace Database\Seeders;

use App\Models\Categoria;
use Database\CategoriaFactory;
use Illuminate\Database\Seeder;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Categoria::factory(100)->create();
    }
}
