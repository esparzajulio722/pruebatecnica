<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->name(),
            'categoria_id' => rand(1, 100),
            'descripcion' => $this->faker->paragraph(),
            'cantidad' => $this->faker->randomFloat($nbMaxDecimals = 0, $min = 0, $max = 100),
            'sku' => $this->faker->word(),
            'precio' => $this->faker->randomFloat($nbMaxDecimals = 0, $min = 0, $max = 100),
            'cantidad' => $this->faker->randomFloat($nbMaxDecimals = 0, $min = 0, $max = 10),
            'estado' => 'C'
        ];
    }
}
