<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    public function index(){
        $productos = Producto::where('estatus','A')->get();

        return response()->json(['data' => $productos], 200);
    }

    public function producto($id){
        $producto = Producto::find($id);

        if($producto == null){
            return response()->json(['message'=>'Producto inexistente.'],400);
        }
        
        return response()->json(['producto' => $producto], 200);
    }

    public function registro(Request $request){
        $data = $request->all();
        $producto = new Producto();
        $producto -> nombre = $data["nombre"];
        $producto -> sku = $data["sku"];
        $producto -> descripcion = $data["descripcion"];
        $producto -> categoria_id = $data["categoria_id"];
        $producto -> cantidad = $data["cantidad"];
        $producto -> precio = $data["precio"];
        $producto -> estado = $data["estado"];
        $producto -> save();
        
        return response()->json(['message'=>'Producto registrado.'],200);
    }

    public function eliminar($id){
        $producto = Producto::find($id);

        if($producto == null){
            return response()->json(['message'=>'Producto inexistente.'],400);
        }
        $producto -> estatus = 'I';
        $producto -> save();
        
        return response()->json(['message'=>'Producto eliminado.'],200);
    }

    public function actualizar(Request $request){
        $data = $request->all();
        $producto = Producto::find($data["id"]);

        if($producto == null){
            return response()->json(['message'=>'Producto inexistente.'],400);
        }
        if(isset($data["nombre"]))
            $producto -> nombre = $data["nombre"];
        if(isset($data["sku"]))
            $producto -> sku = $data["sku"];
        if(isset($data["descripcion"]))
            $producto -> descripcion = $data["descripcion"];
        if(isset($data["categoria_id"]))
            $producto -> categoria_id = $data["categoria_id"];
        if(isset($data["cantidad"]))
            $producto -> cantidad = $data["cantidad"];
        if(isset($data["precio"]))
            $producto -> precio = $data["precio"];
        if(isset($data["estado"]))
            $producto -> estado = $data["estado"];
        $producto -> save();
        
        return response()->json(['message'=>'Producto actualizado.'],200);
    }
}
