<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;

class CategoriasController extends Controller
{
    public function index(){
        $categorias = Categoria::where('estatus','A')->get();

        return response()->json(['data' => $categorias], 200);
    }
}