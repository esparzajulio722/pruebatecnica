<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/categorias', [
    'uses' => 'App\Http\Controllers\CategoriasController@index',
    'as' => 'apiIndexCategorias'
]);

Route::get('/productos', [
    'uses' => 'App\Http\Controllers\ProductosController@index',
    'as' => 'apiIndex'
]);

Route::get('/producto/{id}', [
    'uses' => 'App\Http\Controllers\ProductosController@producto',
    'as' => 'apiProducto'
]);

Route::post('/registro', [
    'uses' => 'App\Http\Controllers\ProductosController@registro',
    'as' => 'apiRegistro'
]);

Route::delete('/eliminar/{id}', [
    'uses' => 'App\Http\Controllers\ProductosController@eliminar',
    'as' => 'apiEliminar'
]);

Route::post('/actualizar', [
    'uses' => 'App\Http\Controllers\ProductosController@actualizar',
    'as' => 'apiActualizar'
]);